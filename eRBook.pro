TEMPLATE = app
QT += network sql
VERSION = 1.1.1
INCLUDEPATH += src
CODECFORTR = UTF-8
#TRANSLATIONS += \
#    translations/sk_SK.ts \
#    translations/cs_CZ.ts

#folder_01.source = qml/eRBook
#folder_01.target = qml
#DEPLOYMENTFOLDERS = folder_01


SOURCES += main.cpp \
    src/lib/oauth.cpp \
    src/json/jsonparser.cpp \
    src/json/json.cpp \
    src/manager.cpp \
    src/settings.cpp \
    src/helper.cpp \
    src/modelitem.cpp \
    src/model.cpp \
    src/sortmodel.cpp \
    src/factory.cpp

HEADERS += \
    src/lib/oauth.h\
    src/json/json.h \
    src/manager.h \
    src/settings.h \
    src/helper.h \
    src/modelitem.h \
    src/model.h \
    src/sortmodel.h \
    src/factory.h

simulator:{
    folder_01.source = qml/eRBook
    folder_01.target = qml
    DEPLOYMENTFOLDERS = folder_01
    RESOURCES += symbian.qrc harmattan.qrc

    DEFINES += APP_VERSION=\\\"$$VERSION\\\"
}


contains(MEEGO_EDITION,harmattan){
    message(MeeGo build)
    TARGET = eRBook
    SOURCES *= src/shareui.cpp
    HEADERS *= src/shareui.h
    database.path = /home/user/$${TARGET}
    #database.files += database/erbook.sqlite
    splash.path = /usr/share/$${TARGET}/
    splash.files = splash/splash_portrait.jpg splash/splash_landscape.jpg
    #plugin.path = /usr/lib/share-ui/plugins
    #plugin.files = LIBRARY/libeRBook.so
    INSTALLS += splash
    CONFIG += qdeclarative-boostable shareuiinterface-maemo-meegotouch mdatauri
    DEFINES += APP_VERSION=\\\"$$VERSION\\\" Q_OS_HARMATTAN
    RESOURCES += harmattan.qrc
}


symbian:{
    message(Symbian build)
    CONFIG += qt-components
    #TARGET = eRBook_0xE2BF62F7
    TARGET = eRBook_0x2006F07E
    # Publish the app version to source code.
    DEFINES += APP_VERSION=\"$$VERSION\"
    ICON = eRBook.svg
    TARGET.UID3 = 0x2006F07E
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x1000 0x2000000 # 32MB
    TARGET.CAPABILITY += NetworkServices \
                         SwEvent \
                         ReadUserData \
                         WriteUserData \
                         LocalServices \
                         UserEnvironment
    # Backup and restore functionality
    #database.sources = database/erbook.sqlite
    #database.path = !:/private/2006F07E
    DEPLOYMENT.display_name = eRBook
    my_deployment.pkg_prerules += vendorinfo
    vendorinfo += "%{\"DevPDA\"}" ":\"DevPDA\""
    DEPLOYMENT += my_deployment
    RESOURCES += symbian.qrc
}


# The files containing the strings to be translated need to be defined under
# sources. Otherwise lupdate tool will not work properly.
#for_lupdate {
#    SOURCES += \
#        qml/eRBook/harmattan/*.qml \
#        qml/eRBook/common/*.qml \
#        qml/eRBook/harmattan/Delegate/*.qml \
#        qml/eRBook/harmattan/Component/*.qml \
#        qml/eRBook/harmattan/ListComp/*.qml \
#        qml/eRBook/symbian/ListComp/*.qml \
#        qml/eRBook/symbian/Component/*.qml \
#        qml/eRBook/symbian/Delegate/*.qml \
#        qml/eRBook/symbian/*.qml
#}


# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()


OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog
