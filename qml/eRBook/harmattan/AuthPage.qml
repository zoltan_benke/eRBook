// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "Component"
import "../scripts/UIConstants.js" as Const
import "../scripts/readability.js" as Readability


MyPage{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: window.pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
        }
        ToolIcon{
            platformIconId: "toolbar-refresh"
            onClicked: webView.url = webUrl
        }
    }

    property url webUrl
    onWebUrlChanged: webView.url = webUrl

    headerText: qsTr("Sign In")


    Connections{
        target: api
        onAccessTokenDone: {
            window.pageStack.push(Qt.resolvedUrl("ListPage.qml"), {loading: true})
        }
    }


    FlickableWeb{
        id: webView
        opacity: webView.progress!==1.0 ? 0 : 1
        anchors {
            top: parent.top
            topMargin: headerMargin
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true
        onUrlChanged: {
            console.debug(url)
            if (url.toString().indexOf("oauth_verifier") !== -1){
                //Readability.urlChanged(url)
                api.accessToken(url)
            }
        }

        Behavior on opacity { PropertyAnimation {duration: pro.visible ? 100 : 400}}
    }

    ProgressBar {
        id: pro
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        value: webView.progress
        visible: webView.progress!==1.0
    }
}
