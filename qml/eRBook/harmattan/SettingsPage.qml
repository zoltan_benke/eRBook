// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "Component"
import "Delegate"
import "../common"
import "../scripts/UIConstants.js" as Const


MyPage{
    id: root

    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: root.pageStack.pop()
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: if (menu.status === DialogStatus.Open) {menu.close()} else {menu.open()}
        }
    }

    headerText: qsTr("Settings")
    property bool parseDone: false

    onStatusChanged: {
        if (status == PageStatus.Activating){
            if (db.font === Const.FONT_LSMALL){
                fontSizeOptions.checkedButton = smallSize
            }
            else if (db.font === Const.FONT_SLARGE){
                fontSizeOptions.checkedButton = mediumSize
            }
            else{
                fontSizeOptions.checkedButton = largeSize
            }
            if (bModel.sortingBy === "title"){
                orderButtons.checkedButton = titleBtn
            }
            else if (bModel.sortingBy === "favorite"){
                orderButtons.checkedButton = favoriteBtn
            }
            else{
                orderButtons.checkedButton = archiveBtn
            }
            if (bModel.sortingOrder === "ascending"){
                sortorderButtons.checkedButton = ascendingBtn
            }
            else{
                sortorderButtons.checkedButton = descendingBtn
            }
            api.userInfo();
        }
    }


    Menu{
        id: menu
        MenuLayout{
            MenuItem{
                enabled: !db.dbEmpty
                text: qsTr("Refresh cache")
                onClicked: db.restoreDB()
            }
        }
    }

    InfoBanner{
        id: banner
        topMargin: Const.MARGIN_XLARGE
    }


    Connections{
        target: api
        onUserInfoDone: {
            try{
                var json = JSON.parse(result)
                username.text = json.username
            }
            catch(err){
                console.debug("UNABLE PARSE")
            }
            parseDone = true
        }
    }

    Connections{
        target: db
        onDbRestored: {
            bModel.clear()
            banner.text = qsTr("Cache restored")
            banner.show()
        }
        onAuthRestored: {
            bModel.clear()
            root.pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
        }
    }


    Flickable{
        id: flick
        width: parent.width - (Const.PADDING_MEDIUM*2)
        anchors {horizontalCenter: parent.horizontalCenter; top: parent.top; bottom: parent.bottom; margins: Const.PADDING_MEDIUM;
            topMargin: headerMargin}
        contentHeight: optionsCol.height
        clip: true;


        Column{
            id: optionsCol
            spacing : 8
            anchors {left: parent.left; right: parent.right;}
            width: parent.width


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("User info");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Username")
                    font.pixelSize: Const.FONT_SLARGE
                    width: parent.width - username.width-5
                }

                Label{
                    id: username
                    opacity: parseDone ? 1 : 0
                    anchors.verticalCenter: parent.verticalCenter
                    elide: Text.ElideMiddle
                    font.pixelSize: Const.FONT_SLARGE
                }


                Behavior on opacity {PropertyAnimation{duration: 250}}
            }

            Button {
                id: logOut
                //: LogOut button
                text: qsTr("Sign Out")
                anchors {horizontalCenter: parent.horizontalCenter}
                onClicked: db.restoreAuth();
            }



            Separator{}


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("Sync settings");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Auto sync <font size=\"2\">(startup, refresh cache)</font>");
                    font.pixelSize: Const.FONT_SLARGE
                    width: parent.width - syncSwitch.width-5
                }

                Switch {
                    id: syncSwitch
                    checked: db.sync
                    onCheckedChanged: {
                        db.sync = checked
                    }
                }
            }



            Separator{}


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("Theme settings");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Night mode");
                    font.pixelSize: Const.FONT_SLARGE
                    width: parent.width - themeSwitch.width-5
                }

                Switch {
                    id: themeSwitch
                    checked: db.theme
                    onCheckedChanged: {
                        db.theme = checked
                        theme.inverted = db.theme
                    }
                }
            }

            Separator{}

            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("Article settings");
            }

            Item {
                id: fontSizeCriteria
                width: parent.width
                height: childrenRect.height

                Text {
                    id: fontSizeText
                    anchors.top:  parent.top
                    text: qsTr("Font size")
                    font.pixelSize: Const.FONT_SLARGE
                    font.family: Const.FONT_FAMILY
                    color: !theme.inverted ? Const.COLOR_FOREGROUND : Const.COLOR_INVERTED_FOREGROUND
                }

                ButtonColumn {
                    id: fontSizeOptions
                    anchors { top: fontSizeText.top; right: parent.right }
                    anchors.topMargin: Const.DEFAULT_MARGIN / 2

                    Button {
                        id: smallSize
                        //: Small
                        text: qsTr("Small")
                        onClicked: db.font = Const.FONT_LSMALL
                    }
                    Button {
                        id: mediumSize
                        //: Medium
                        text: qsTr("Medium")
                        onClicked: db.font = Const.FONT_SLARGE
                    }
                    Button {
                        id: largeSize
                        //: Large
                        text: qsTr("Large")
                        onClicked: db.font = Const.FONT_XLARGE
                    }
                }
            }


            Separator{}

            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("Sorting settings");
            }


            Item {
                id: orderSettings
                width: parent.width
                height: childrenRect.height

                Text {
                    id: orderText
                    anchors.top:  parent.top
                    text: qsTr("Order by")
                    font.pixelSize: Const.FONT_SLARGE
                    font.family: Const.FONT_FAMILY
                    color: !theme.inverted ? Const.COLOR_FOREGROUND : Const.COLOR_INVERTED_FOREGROUND
                }

                ButtonColumn {
                    id: orderButtons
                    anchors { top: orderText.top; right: parent.right }
                    anchors.topMargin: Const.DEFAULT_MARGIN / 2

                    Button {
                        id: titleBtn
                        //: Small
                        text: qsTr("Title")
                        onClicked: {
                            db.sortBy = "title"
                        }
                    }
                    Button {
                        id: favoriteBtn
                        //: Medium
                        text: qsTr("Favorite")
                        onClicked: {
                            db.sortBy = "favorite"
                        }
                    }
                    Button {
                        id: archiveBtn
                        //: Large
                        text: qsTr("Readed")
                        onClicked: {
                            db.sortBy = "archive"
                        }
                    }
                }
            }


            Item {
                id: sortOrderSettings
                width: parent.width
                height: childrenRect.height

                Text {
                    id: sortOrderText
                    anchors.top:  parent.top
                    text: qsTr("Sort order")
                    font.pixelSize: Const.FONT_SLARGE
                    font.family: Const.FONT_FAMILY
                    color: !theme.inverted ? Const.COLOR_FOREGROUND : Const.COLOR_INVERTED_FOREGROUND
                }

                ButtonColumn {
                    id: sortorderButtons
                    anchors { top: sortOrderText.top; right: parent.right }
                    anchors.topMargin: Const.DEFAULT_MARGIN / 2

                    Button {
                        id: ascendingBtn
                        //: Small
                        text: qsTr("Ascending")
                        onClicked: {
                            db.sortOrder = "ascending"
                        }
                    }
                    Button {
                        id: descendingBtn
                        //: Medium
                        text: qsTr("Descending")
                        onClicked: {
                            db.sortOrder = "descending"
                        }
                    }
                }
            }

            Separator{}


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: Const.FONT_XLARGE
                text: qsTr("Debugging");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Show server side errors");
                    font.pixelSize: Const.FONT_SLARGE
                    width: parent.width - debugSwitch.width-5
                }

                Switch {
                    id: debugSwitch
                    checked: db.debug
                    onCheckedChanged:{
                        db.debug = checked
                    }
                }
            }
        }

    }
}
