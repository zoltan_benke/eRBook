import QtQuick 1.1

Item {
    id: splashScreen




    Image {
        anchors.fill: parent
        source: "../images/background_white_text.png"
    }

    Column {
        height: childrenRect.height
        anchors.centerIn: parent
        spacing: 10

        Image{
            source: "../images/logo.png"
            height: 150
            width: 150
            sourceSize.width: width
            sourceSize.height: height
            smooth: true
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 45
            text: qsTr("eRBook %1").arg(appVersion)
        }
    }


    Behavior on opacity { NumberAnimation { duration: 350 } }
}
