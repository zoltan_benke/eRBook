import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/UIConstants.js" as Const

Dialog {
    id: dialog

    property alias errorCode: codeLabel.text
    property alias errorText: textLabel.text

    width: parent.width
    content: Item {
        height: column.height
        anchors { left: parent.left; right: parent.right; margins: 10 }

        Column {
            id: column

            anchors { left: parent.left; right: parent.right; top: parent.top }
            spacing: 10

            Image {
                id: icon

                x: Math.floor((parent.width / 2) - (width / 2))
                source: "../../images/error.png"
            }

            Label {
                id: codeLabel

                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                font.pixelSize: Const.FONT_LARGE
                color: "red"
            }

            Label {
                id: textLabel

                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                color: "white"
            }
        }
    }
}
