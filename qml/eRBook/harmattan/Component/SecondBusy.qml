// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/UIConstants.js" as Const

Item{
    id: root
    z: 1000


    property bool show: false
    property alias textLabel: label.text


    BusyIndicator {
        id: busy
        anchors {centerIn: parent}
        running: visible
        visible: show
        platformStyle: BusyIndicatorStyle {size: "large"}
    }


    Label{
        id: label
        visible: busy.visible
        anchors {horizontalCenter: parent.horizontalCenter; top: busy.bottom; topMargin: Const.PADDING_MEDIUM}
        font.pixelSize: Const.FONT_XLARGE
    }
}
