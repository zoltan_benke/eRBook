// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/UIConstants.js" as Const
import "../Component"

Page{
    id: root


    property bool loading: false
    property alias headerShow: header.visible
    property alias headerText: titleHeader.text
    property alias headerHeight: header.height
    property alias headerColor: header.color
    property int headerMargin: headerShow ? header.height : 0
    property bool isFavorite: false
    property bool isArchive: false


    property alias workingText: working.textLabel
    property bool workingShow: false



    Rectangle{
        id: header
        width: parent.width
        height: inPortrait ? Const.HEADER_DEFAULT_HEIGHT_PORTRAIT : Const.HEADER_DEFAULT_HEIGHT_LANDSCAPE
        z: 1000
        smooth: true
        gradient: Gradient{
            GradientStop {position: 0.4; color: !theme.inverted ? "#980000" : "#454444"}
            GradientStop {position: 0.9; color: !theme.inverted ? "#860000" : "#3b3b3b"}
        }



        Label{
            id: titleHeader
            anchors {left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter; margins: Const.PADDING_MEDIUM}
            elide: Text.ElideLeft
            color: theme.inverted ? "white" : Const.COLOR_INVERTED_FOREGROUND
            font.pixelSize: inPortrait ? Const.FONT_LARGE : Const.FONT_DEFAULT
            font.bold: true

        }
    }


    SecondBusy{
        id: working
        anchors {centerIn: parent}
        show: workingShow
    }


    BusyIndicator {
        id: busy
        anchors {horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: parent.height/2}
        running: visible
        visible: loading
        platformStyle: BusyIndicatorStyle {size: "large"}
    }
}
