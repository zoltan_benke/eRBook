import QtQuick 1.1
import com.nokia.meego 1.1


import "../../scripts/UIConstants.js" as Const

QueryDialog {
    id: dialog


    property alias text: textPrivacy.text
    property alias title: dialog.titleText

    height: contentItem.height
    content: Item {
        id: contentItem

        height: column.height
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 5 }

        Column {
            id: column

            anchors { top: parent.top; left: parent.left; right: parent.right }
            spacing: 20

            Label {
                id: textPrivacy
                width: parent.width
                color: "white"
                wrapMode: Text.WordWrap
            }
        }
    }
}
