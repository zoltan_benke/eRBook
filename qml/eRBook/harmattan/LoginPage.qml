// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "Component"
import "../common"
import "../scripts/UIConstants.js" as Const
import "../scripts/oauth.js" as Auth
import "../scripts/createobject.js" as Dynamic

MyPage{
    id: root
    headerText: qsTr("Sign In")


    function login(user, pass){
        var xhr = Auth.createOAuthHeader("POST", "https://www.readability.com/api/rest/v1/oauth/access_token",
                                         [["x_auth_username", user], ["x_auth_password", pass], ["x_auth_mode", "client_auth"]]);
        xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4){
                        if (xhr.status === 200) {
                            var response = xhr.responseText.split('&');
                            console.debug(xhr.responseText+"\nTOKEN: "+response[1].split('=')[1]+"\nSECRET: "+response[0].split('=')[1])
                            db.setAuth(response[1].split('=')[1], response[0].split('=')[1])
                            root.pageStack.replace(Qt.resolvedUrl("ListPage.qml"), {workingText: qsTr("Syncing"), workingShow: true})
                        }
                        else {
                            if (xhr.status === 401){
                                banner.text = qsTr("Invalid credentials")
                                workingShow = false
                                banner.show()
                                username.errorHighlight = true
                                password.errorHighlight = true
                                invalidTimer.start()
                            }

                            console.debug(xhr.status + "\n" + xhr.statusText)
                        }
                    }
                }
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xhr.setRequestHeader("Accept-Language", "en");
        xhr.setRequestHeader("User-Agent", "Mozilla/5.0")
        xhr.send();
    }



    function showPrivacy(){
        var dialog = Dynamic.createObject(Qt.resolvedUrl("Component/PrivacyDialog.qml"), root.pageStack)
        dialog.text = qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.")
        dialog.title = qsTr("Privacy policy")
        dialog.open()
    }


    onStatusChanged: {
        if (status == PageStatus.Activating){
            username.text = ""
            password.text = ""
        }
    }


    Timer{
        id: invalidTimer
        interval: 2000
        onTriggered: {
            username.errorHighlight = false
            password.errorHighlight = false
            invalidTitle.visible = false
        }
    }


    Flickable{
        opacity: workingShow ? 0.2 : 1
        anchors {fill: parent; topMargin: headerMargin; margins: Const.PADDING_MEDIUM}
        flickableDirection: Flickable.VerticalFlick
        contentHeight: content.height


        Behavior on opacity {PropertyAnimation{duration: workingShow ? 100 : 250}}

        Column{
            id: content
            anchors {left: parent.left; right: parent.right}
            spacing: 10


            Image{
                anchors {horizontalCenter: parent.horizontalCenter}
                smooth: true
                source: "../images/logo.png"
                width: sourceSize.width
                height: sourceSize.height
                sourceSize.width: 100
                sourceSize.height: 100
            }


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                text: qsTr("eRBook %1").arg(appVersion)
                font.pixelSize: Const.FONT_XLARGE
            }

            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width
                wrapMode: Text.WordWrap
                text: qsTr("need access to you Readability account. Fill the fields and click on Sign In button.")
                font.italic: true
            }


            TextField{
                id: username
                width: parent.width
                placeholderText: qsTr("Username")
                echoMode: TextInput.Normal
                validator: RegExpValidator{regExp: /[^\\//|]*/}
                Keys.onUpPressed: signIn.focus = true;
                Keys.onDownPressed: password.focus = true;
                Keys.onReturnPressed: password.focus = true;
            }



            TextField{
                id: password
                width: parent.width
                placeholderText: qsTr("Password")
                echoMode: TextInput.Password
                validator: RegExpValidator{regExp: /[^\\//|]*/}
                Keys.onUpPressed: username.focus = true;
                Keys.onDownPressed: signIn.focus = true;
                Keys.onReturnPressed: signIn.clicked;
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: signIn
                    text: qsTr("Sign In")
                    width: parent.width - signUp.width
                    onClicked: {
                        if (username.text != "" && password.text != ""){
                            workingText = qsTr("Please wait")
                            workingShow = true
                            login(username.text, password.text)
                        }
                    }
                }

                Button{
                    id: signUp
                    width: parent.width/2
                    text: qsTr("Sign Up")
                    onClicked: Qt.openUrlExternally("https://www.readability.com/readers/register")
                }
            }

            Separator{}

            Label{
                id: privacy
                anchors {horizontalCenter: parent.horizontalCenter;}
                text: qsTr("<a style='color:red' href='http://www.slavnecitaty.eu'>Privacy policy</a>")
                font.pixelSize: Const.FONT_SLARGE
                font.bold: true
                onLinkActivated: {
                    //privacyDialog.text = qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.")
                    //privacyDialog.title = qsTr("Privacy policy")
                    //privacyDialog.open()
                    showPrivacy()
                }
            }
        }
    }
}
