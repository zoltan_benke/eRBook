import QtQuick 1.1
import com.nokia.meego 1.1
import QtWebKit 1.0

import "Component"
import "Delegate"
import "../scripts/UIConstants.js" as Const

MyPage {
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: {
                bModel.reSort()
                root.pageStack.pop()
            }
        }
        ToolIcon{
            platformIconId: "toolbar-share"
            onClicked: {
                if (db.shortUrlExists(bModel.cArticleId)){
                    shareUI.share(bModel.cTitle, db.shortUrl(bModel.cArticleId))
                }else{
                    workingText = qsTr("Share")
                    workingShow = true
                    api.shortUrl(bModel.cUrl)
                }
            }
        }
        ToolIcon{
            platformIconId: bModel.cFavorite ? "toolbar-favorite-mark" : "toolbar-favorite-unmark"
            onClicked: {
                if (bModel.cFavorite){
                    workingText = qsTr("Remove from favorite")
                }else{
                    workingText = qsTr("Add to favorite")
                }
                workingShow = true
                api.updateBookmark(bModel.cFavorite ? 0 : 1, bModel.cArchive ? 1 : 0)

            }
        }
        ToolIcon{
            //iconSource: !theme.inverted ? "../images/readed_black.svg" : "../images/readed_white.svg"
            platformIconId: bModel.cArchive ? "toolbar-history" : "toolbar-done"
            onClicked: {
                if (bModel.cArchive){
                    workingText = qsTr("Set as unread")
                }else{
                    workingText = qsTr("Set as read")
                }
                workingShow = true
                api.updateBookmark(bModel.cFavorite ? 1 : 0, bModel.cArchive ? 0 : 1)

            }
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: {
                if (menu.status === DialogStatus.Open)  { menu.close() } else { menu.open() }
            }
        }
    }

    headerText: bModel.cTitle
    property string contentStyle: !theme.inverted ? "<style>body{background: #e0e1e2}</style><font color='black'>" :
                                                    "<style>body{background: #000000}</style><font color='white'>"

    onStatusChanged: {
        if (status == PageStatus.Active){
            content.settings.defaultFontSize = db.font
            textView.contentY = parseInt(db.position(bModel.cArticleId))
            if (db.contentExists(bModel.cArticleId)){
                console.debug("DATA FROM CACHER")
                content.html = contentStyle+db.content(bModel.cArticleId)
                workingShow = false
            }else{
                console.debug("DATA FROM INTERNET")
                api.readBookmark()
            }
        }
    }


    Menu{
        id: menu
        MenuLayout{
            MenuItem{
                text: qsTr("Open in browser")
                onClicked: {
                    menu.close()
                    Qt.openUrlExternally(bModel.cUrl)
                }
            }
        }
    }


    Connections{
        target: api
        onReadBookmarkDone: {
            try{
                var json = JSON.parse(result)
                var obsah = json.content.replace(new RegExp("href=", "gm"), "")
                db.setBookmark(bModel.cArticleId, obsah)
                content.html = contentStyle+db.content(bModel.cArticleId)
            }
            catch (err){
                console.debug("UNABLE PARSE")
            }
            workingShow = false
        }
    }


    Flickable{
        id: textView
        clip: true;
        width: parent.width
        opacity: workingShow ? 0 : 1
        anchors{
            top: parent.top;
            topMargin: headerMargin
            bottom: parent.bottom;
            left: parent.left;
            right: parent.right;
        }

        onMovementEnded: {
            db.setPosition(bModel.cArticleId, contentY)
        }

        contentWidth: Math.max(parent.width,content.width);
        contentHeight: Math.max(parent.height,content.height);

        Behavior on contentY{
            NumberAnimation { duration: 400 }
        }

        Behavior on opacity {PropertyAnimation{duration: workingShow ? 100 : 600}}

        WebView{
            id: content
            preferredWidth: textView.width
            preferredHeight: textView.height;
            scale: 1
            contentsScale: 1
            settings.javascriptCanAccessClipboard: true;
            settings.javascriptEnabled : true;
            settings.autoLoadImages : true;
            settings.linksIncludedInFocusChain: true;
            settings.offlineStorageDatabaseEnabled : true
            settings.offlineWebApplicationCacheEnabled : true
            settings.localStorageDatabaseEnabled : true
        }
    }

    ScrollDecorator  {
        flickableItem: textView
        anchors { right: textView.right; top: textView.top }
    }

}

