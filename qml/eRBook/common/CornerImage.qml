// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Item {
    z: 100000
    height: 15
    anchors { top: parent.top; left: parent.left; right: parent.right }

    Image {
        anchors { top: parent.top; left: parent.left }
        source: "../images/corner_left.png"
    }

    Image {
        anchors { top: parent.top; right: parent.right }
        source: "../images/corner_right.png"
    }
}
