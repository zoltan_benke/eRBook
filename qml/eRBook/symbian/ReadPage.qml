import QtQuick 1.1
import com.nokia.symbian 1.1
import QtWebKit 1.0

import "Component"
import "Delegate"
import "../scripts/UIConstants.js" as Const

MyPage {
    id: root
    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: {
                bModel.reSort()
                root.pageStack.pop()
            }
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-share"
            onClicked: {
                if (db.shortUrlExists(bModel.cArticleId)){
                    shareContext.open()
                }else{
                    workingText = qsTr("Share")
                    workingShow = true
                    api.shortUrl(bModel.cUrl)
                }
            }
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: platformInverted ? bModel.cFavorite ? "../images/favourite_inverted_yes.svg" :
                                                              "../images/favourite_inverted_no.png" :
            bModel.cFavorite ? "../images/favourite_yes.svg" :
                "../images/favourite_no.png"
            onClicked: {
                if (bModel.cFavorite){
                    workingText = qsTr("Remove from favorite")
                }else{
                    workingText = qsTr("Add to favorite")
                }
                workingShow = true
                api.updateBookmark(bModel.cFavorite ? 0 : 1, bModel.cArchive ? 1 : 0)

            }
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: platformInverted ? bModel.cArchive ? "../images/unreaded_black.png" :
                                                             "../images/readed_black.svg" :
            bModel.cArchive ? "../images/unreaded_white.png" :
                "../images/readed_white.svg"
            onClicked: {
                if (bModel.cArchive){
                    workingText = qsTr("Set as unread")
                }else{
                    workingText = qsTr("Set as read")
                }
                workingShow = true
                api.updateBookmark(bModel.cFavorite ? 1 : 0, bModel.cArchive ? 0 : 1)

            }
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-menu"
            onClicked: if (menu.status === DialogStatus.Open)  { menu.close() } else { menu.open() }
        }
    }

    headerText: bModel.cTitle
    property string contentStyle: window.platformInverted ? "<style>body{background: #f0f0f0}</style><font color='black'>" :
                                                            "<style>body{background: #000000}</style><font color='white'>"

    onStatusChanged: {
        if (status == PageStatus.Active){
            content.settings.defaultFontSize = db.font
            textView.contentY = parseInt(db.position(bModel.cArticleId))
            if (db.contentExists(bModel.cArticleId)){
                console.debug("DATA FROM CACHER")
                content.html = contentStyle+db.content(bModel.cArticleId)
                workingShow = false
            }else{
                console.debug("DATA FROM INTERNET")
                api.readBookmark()
            }
        }
    }


    Menu{
        id: menu
        platformInverted: window.platformInverted
        MenuLayout{
            MenuItem{
                platformInverted: window.platformInverted
                text: qsTr("Open in browser")
                onClicked: {
                    menu.close()
                    Qt.openUrlExternally(bModel.cUrl)
                }
            }
        }
    }


    ShareContext{
        id: shareContext
    }


    Connections{
        target: api
        onReadBookmarkDone: {
            try{
                var json = JSON.parse(result)
                var obsah = json.content.replace(new RegExp("href=", "gm"), "")
                db.setBookmark(bModel.cArticleId, obsah)
                content.html = contentStyle+db.content(bModel.cArticleId)
            }
            catch (err){
                console.debug("UNABLE PARSE")
            }
            workingShow = false
        }
        onShortUrlDone: shareContext.open()
    }


    Flickable{
        id: textView
        clip: true;
        width: parent.width
        opacity: (loading) || workingShow ? 0 : 1
        anchors{
            top: parent.top;
            topMargin: headerMargin
            bottom: parent.bottom;
            left: parent.left;
            right: parent.right;
        }

        onMovementEnded: {
            db.setPosition(bModel.cArticleId, contentY)
        }

        contentWidth: Math.max(parent.width,content.width);
        contentHeight: Math.max(parent.height,content.height);

        Behavior on contentY{
            NumberAnimation { duration: 400 }
        }

        Behavior on opacity {PropertyAnimation{duration: workingShow ? 100 : 500}}

        WebView{
            id: content
            preferredWidth: textView.width
            preferredHeight: textView.height;
            scale: 1
            contentsScale: 1
            settings.javascriptCanAccessClipboard: true;
            settings.javascriptEnabled : true;
            settings.autoLoadImages : true;
            settings.linksIncludedInFocusChain: true;
            settings.offlineStorageDatabaseEnabled : true
            settings.offlineWebApplicationCacheEnabled : true
            settings.localStorageDatabaseEnabled : true
        }
    }

    ScrollDecorator  {
        flickableItem: textView
        anchors { right: textView.right; top: textView.top }
    }

}

