import QtQuick 1.1
import com.nokia.symbian 1.1

import "Component"
import "Delegate"
import "../common"


MyPage{
    id: root
    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: root.pageStack.pop()
        }
    }


    onStatusChanged: {
        if (status == PageStatus.Active){

        }
    }



    headerText: qsTr("Add article")


    Connections{
        target: api
        onErrorAddBookmark: {
            workingShow = false
        }

        onAddBookmarkDone: {
            api.bookmarks()
        }
        onBookmarksDone:{
            workingShow = false
            root.pageStack.pop()
        }
    }



    Flickable{
        id: flicker
        enabled: !workingShow
        opacity: workingShow ? 0.2 : 1
        anchors {fill: parent; topMargin: headerMargin; leftMargin: platformStyle.paddingMedium; rightMargin: platformStyle.paddingMedium}

        flickableDirection: Flickable.VerticalFlick
        contentHeight: articleUrlTitle.height +
                       urlEdit.height +
                       urlPaste.height +
                       separe.height +
                       favoriteLabel.height +
                       favRadio.height +
                       separe2.height +
                       archiveLabel.height +
                       archRadio.height +
                       separe3.height +
                       addArticle.height +
                       (platformStyle.paddingMedium*11)

        clip: true


        Behavior on opacity {PropertyAnimation{duration: workingShow ? 150 : 200}}


        Label{
            id: articleUrlTitle
            platformInverted: window.platformInverted
            anchors {top: parent.top; left: parent.left; right: parent.right; topMargin: platformStyle.paddingMedium}
            font.pixelSize: platformStyle.fontSizeLarge
            text:  qsTr("Article Url");
        }

        TextField{
            id: urlEdit
            platformInverted: window.platformInverted
            anchors {left: parent.left; right: parent.right; top: articleUrlTitle.bottom; topMargin: platformStyle.paddingMedium}
            placeholderText:  qsTr("required")
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhPreferLowercase | Qt.ImhLowercaseOnly
        }

        Button{
            id: urlPaste
            platformInverted: window.platformInverted
            anchors {right: parent.right; top: urlEdit.bottom; topMargin: platformStyle.paddingMedium}
            text: {
                if (urlEdit.text === ""){
                    return qsTr("Paste Url")
                }else{
                    return qsTr("Clear")
                }
            }
            onClicked: {
                if (urlEdit.text === ""){
                    urlEdit.text = helper.getText()
                }else{
                    urlEdit.text = ""
                }
            }
        }

        Separator{
            id: separe
            anchors {top: urlPaste.bottom; topMargin: platformStyle.paddingMedium}
        }


        Label{
            id: favoriteLabel
            platformInverted: window.platformInverted
            anchors {left: parent.left; right: parent.right; top: separe.bottom; topMargin: platformStyle.paddingMedium}
            font.pixelSize: platformStyle.fontSizeLarge
            text:  qsTr("Favorite");
        }


        Switch{
            id: favRadio
            platformInverted: window.platformInverted
            anchors {right: parent.right; top: favoriteLabel.bottom; topMargin: platformStyle.paddingMedium}
        }


        Image{
            id: favoriteImage
            anchors {left: parent.left; verticalCenter: favRadio.verticalCenter}
            source: window.platformInverted ? favRadio.checked ? "../images/favourite_inverted_yes.svg" :
                                                 "../images/favourite_inverted_no.png" :
                                      favRadio.checked ? "../images/favourite_yes.svg" :
                                                 "../images/favourite_no.png"
        }


        Separator{
            id: separe2
            anchors {top: favRadio.bottom; topMargin: platformStyle.paddingMedium}
        }


        Label{
            id: archiveLabel
            platformInverted: window.platformInverted
            anchors {left: parent.left; top: separe2.bottom; topMargin: platformStyle.paddingMedium}
            font.pixelSize: platformStyle.fontSizeLarge
            text:  qsTr("Archive");
        }


        Switch{
            id: archRadio
            platformInverted: window.platformInverted
            anchors {right: parent.right; verticalCenter: archiveLabel.verticalCenter}
        }


        Separator{
            id: separe3
            anchors {top: archRadio.bottom; topMargin: platformStyle.paddingMedium}
        }


        Button{
            id: addArticle
            platformInverted: window.platformInverted
            anchors {horizontalCenter: parent.horizontalCenter; top: separe3.bottom; topMargin: platformStyle.paddingMedium}
            text:  qsTr("Add Article")
            width: window.inPortrait ? parent.width/2 : parent.height/2
            onClicked: {
                if( urlEdit.text.length > 0){
                    workingText = qsTr("Adding article")
                    workingShow = true
                    api.addBookmark(urlEdit.text, (favRadio.checked ? 1 : 0), (archRadio.checked ? 1 : 0))
                }else{
                    console.debug("REQUIRED FIELD")
                }
            }
        }
    }
}
