import QtQuick 1.1

Item {
    id: splashScreen




    Image {
        anchors.fill: parent
        source: "../images/background_white.png"
    }

    Column {
        height: childrenRect.height
        anchors.centerIn: parent
        spacing: 5

        Image{
            source: "../images/logo_splash.png"
            height: sourceSize.height
            width: sourceSize.width
            sourceSize.width: 140
            sourceSize.height: 104
            smooth: true
            clip: true
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Text {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 40
            font.bold: true
            text: qsTr("eRBook")
            style: Text.Raised
            styleColor: "#AAAAAA"
        }
    }


    Behavior on opacity { NumberAnimation { duration: 350 } }
}
