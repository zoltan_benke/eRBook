// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

Item{
    id: root
    z: 1000


    property bool show: false
    property alias textLabel: label.text



    BusyIndicator {
        id: busy
        anchors {centerIn: parent}
        running: visible
        width: 70
        height: 70
        visible: show
    }


    Label{
        id: label
        platformInverted: window.platformInverted
        visible: busy.visible
        anchors {horizontalCenter: parent.horizontalCenter; top: busy.bottom; topMargin: platformStyle.paddingMedium}
        font.pixelSize: platformStyle.fontSizeLarge
    }
}
