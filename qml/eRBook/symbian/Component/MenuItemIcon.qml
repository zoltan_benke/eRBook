// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

MenuItem{
    id: root


    property alias iconSource: icon.source

    platformLeftMargin: 2 * platformStyle.paddingMedium + platformStyle.graphicSizeSmall

    Image{
        id: icon

        anchors {
            left: parent.left
            leftMargin: platformStyle.paddingMedium
            verticalCenter: parent.verticalCenter
        }
        sourceSize.width: platformStyle.graphicSizeSmall
        sourceSize.height: platformStyle.graphicSizeSmall
    }
}
