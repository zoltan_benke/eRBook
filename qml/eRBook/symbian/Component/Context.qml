// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


import "../../scripts/createobject.js" as Dynamic

ContextMenu{
    id: root


    platformInverted: window.platformInverted

    property bool favorited
    property bool readed

    signal favoriteClicked
    signal readedClicked
    signal deleteClicked


    MenuLayout {
        MenuItem {
            platformInverted: window.platformInverted
            text: bModel.cFavorite ? qsTr("Remove from favorite") : qsTr("Add to favorite")
            onClicked: root.favoriteClicked()
        }
        MenuItem {
            platformInverted: window.platformInverted
            text: bModel.cArchive ? qsTr("Set as unread") : qsTr("Set as read")
            onClicked: root.readedClicked()
        }
        MenuItem {
            platformInverted: window.platformInverted
            text: qsTr("Remove")
            onClicked: root.deleteClicked()
        }
    }
}
