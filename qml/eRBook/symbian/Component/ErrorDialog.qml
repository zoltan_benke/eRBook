import QtQuick 1.1
import com.nokia.symbian 1.1


QueryDialog {
    id: dialog

    property alias errorCode: dialog.titleText
    property alias errorText: textLabel.text
    onClickedOutside: reject()
    platformInverted: window.platformInverted
    icon: "../../images/error.png"
    titleText: errorCode
    width: parent.width
    content: Item {
        height: column.height+30
        anchors { left: parent.left; right: parent.right; margins: 10 }

        Column {
            id: column

            anchors { left: parent.left; right: parent.right; top: parent.top }
            spacing: 10


            Label {
                id: textLabel
                platformInverted: window.platformInverted
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                //color: "white"
            }
        }
    }
}
