// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


import "../../scripts/createobject.js" as Dynamic

ContextMenu{
    id: root

    platformInverted: window.platformInverted




    MenuLayout {
        MenuItemIcon {
            platformInverted: window.platformInverted
            text: qsTr("Copy Url")
            iconSource: "../../images/clipboard.png"
            onClicked: {
                var url = db.shortUrl(bModel.cArticleId) == "" ? bModel.cUrl : db.shortUrl(bModel.cArticleId)
                console.debug(url)
                helper.setText(url)
            }
        }
        MenuItemIcon {
            platformInverted: window.platformInverted
            text: qsTr("SMS")
            iconSource: "../../images/sms.png"
            onClicked: {
                var url = db.shortUrl(bModel.cArticleId) == "" ? bModel.cUrl : db.shortUrl(bModel.cArticleId)
                var body = bModel.cTitle+"\nshared with #eRBook\n"
                console.debug(body+url)
                helper.sendSms(body+url)
            }
        }
        MenuItemIcon {
            platformInverted: window.platformInverted
            text: qsTr("Email")
            iconSource: "../../images/email.png"
            onClicked: {
                var url = db.shortUrl(bModel.cArticleId) == "" ? bModel.cUrl : db.shortUrl(bModel.cArticleId)
                var body = bModel.cTitle+"\nshared with #eRBook\n"
                console.debug(body+url)
                helper.sendEmail(body+url)
            }
        }
        /*
        MenuItemIcon {
            platformInverted: window.platformInverted
            text: qsTr("Twitter")
            iconSource: "../../images/twitter.png"
            onClicked: {
                var url = db.shortUrl(bModel.cArticleId) == "" ? bModel.cUrl : db.shortUrl(bModel.cArticleId)
                console.debug(url)
                helper.openUrl(url)
            }
        }
        */
    }
}
