// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


SelectionDialog{
    id: root
    titleText: qsTr("View mode")

    platformInverted: window.platformInverted

    onStatusChanged: {
        if (status == DialogStatus.Opening){
            if (db.viewMode == "all"){
                root.selectedIndex = 0
            }else if (db.viewMode == "archive"){
                root.selectedIndex = 1
            }else{
                root.selectedIndex = 2
            }
        }
    }

    model: ListModel {
        id: model
        ListElement { name: "" }
        ListElement { name: "" }
        ListElement { name: "" }
    }

    Component.onCompleted: {
        model.get(0).name = qsTr("All Items")
        model.get(1).name = qsTr("Read items")
        model.get(2).name = qsTr("Unread items")
    }


    onSelectedIndexChanged: {
        console.log(selectedIndex)
        var i = selectedIndex
        switch (i){
        case 0:
            db.viewMode = "all"
            break;
        case 1:
            db.viewMode = "archive"
            break;
        case 2:
            db.viewMode = "non_archive"
            break;
        }
    }


}
