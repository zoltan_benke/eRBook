// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "Component"
import "Delegate"
import "../common"


MyPage{
    id: root

    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: {
                root.pageStack.pop()
            }
        }
    }

    headerText: qsTr("About")


    Image{
        id: devpdaLogo
        z: -1000
        anchors {right: parent.right; bottom: parent.bottom; margins: platformStyle.paddingMedium}
        smooth: true
        source: "../images/devpda.png"
        width: sourceSize.width
        height: sourceSize.height
        sourceSize.width: window.inPortrait ? parent.width/2 : parent.height/2
        sourceSize.height: window.inPortrait ? parent.width/2 : parent.height/2
        opacity: 0.6
    }


    Flickable{
        anchors {fill: parent; topMargin: headerMargin; margins: platformStyle.paddingMedium}
        flickableDirection: Flickable.VerticalFlick
        contentHeight: content.height

        Column{
            id: content
            anchors {left: parent.left; right: parent.right}
            spacing: 5


            Label{
                platformInverted: window.platformInverted
                text: qsTr("eRBook")
                font.pixelSize: platformStyle.fontSizeLarge*1.5
                width: parent.width
            }
            Label{
                platformInverted: window.platformInverted
                text: qsTr("version %1").arg(appVersion)
                font.pixelSize: platformStyle.fontSizeMedium
                width: parent.width
            }

            Label{
                platformInverted: window.platformInverted
                text: qsTr("Copyright (c) 2010-2013 DevPDA<br/><a  href='http://devpda.net'>www.devpda.net</a>")
                font.pixelSize: platformStyle.fontSizeMedium
                width: parent.width
            }

            Separator{}


            Label{
                platformInverted: window.platformInverted
                text: qsTr("A fully featured Readability client for Symbian and MeeGo Smartphones that allows you to take your reading list." );
                font.pixelSize: platformStyle.fontSizeLarge
                width: parent.width
                wrapMode: Text.WordWrap
            }

            Separator {}

            Label{
                platformInverted: window.platformInverted
                text: qsTr("Privacy policy")
                font.pixelSize: platformStyle.fontSizeLarge*1.5
                onLinkActivated: privacyDialog.open()
            }

            Label{
                platformInverted: window.platformInverted
                text: qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.")
                font.pixelSize: platformStyle.fontSizeLarge
                font.italic: true
                width: parent.width
                wrapMode: Text.WordWrap
            }
        }
    }

}
