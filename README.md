# eRBook - Readability client

v1.1.1 - 9.9.2013
• Fixed full offline support. Now syncing also article content. 
• Fixed security issue when entering password 
• Minor fixes


v1.1.0 - 18.02.2013
• Full support for offline reading (cache)
• Auto syncing On/Off
• Added functions like Share article, Open in browser etc...
• Splash screen
• Minor bugs fixed


v1.0.0 - 12.02.2013
• Initial version