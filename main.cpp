#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"


#include "manager.h"
#include "helper.h"
#include "factory.h"
#ifdef Q_OS_HARMATTAN
#include "shareui.h"
#endif

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);


    /* Expose APP VERSION to QML */
    app->setApplicationVersion(APP_VERSION);
    viewer.rootContext()->setContextProperty("appVersion", app->applicationVersion());


    /* Expose helper to QML */
    Helper helper;
    viewer.rootContext()->setContextProperty("helper", &helper);

    Factory *factory = new Factory();
    viewer.engine()->setNetworkAccessManagerFactory(factory); // Ignoring SSL errors

#ifdef Q_OS_HARMATTAN
    ShareUI shareUI;
    viewer.rootContext()->setContextProperty("shareUI", &shareUI);
#endif


    Manager manager;
    viewer.rootContext()->setContextProperty("api", &manager);
    viewer.rootContext()->setContextProperty("db", manager.set);
    viewer.rootContext()->setContextProperty("bModel", manager.sortModel);
    viewer.rootContext()->setContextProperty("auth", manager.auth);



#ifdef Q_OS_SYMBIAN
    viewer.rootContext()->setContextProperty("isSymbian", true);
    //viewer.setMainQmlFile(QLatin1String("qml/eRBook/symbian/main.qml"));
    viewer.setSource(QUrl("qrc:/qml/eRBook/symbian/Init.qml"));
#elif defined(Q_OS_HARMATTAN)
    viewer.rootContext()->setContextProperty("isSymbian", false);
    //viewer.setMainQmlFile(QLatin1String("qml/eRBook/harmattan/main.qml"));
    viewer.setSource(QUrl("qrc:/qml/eRBook/harmattan/main.qml"));
#else
    viewer.rootContext()->setContextProperty("isSymbian", true);
    viewer.setMainQmlFile(QLatin1String("qml/eRBook/symbian/Init.qml"));
    //viewer.setSource(QUrl("qrc:/qml/eRBook/symbian/main.qml"));
#endif
    viewer.showExpanded();

    return app->exec();
}
