#include "model.h"


Model::Model(QObject *parent) :
    QAbstractListModel(parent)

{

    QHash<int, QByteArray> roles;
    roles[FAVORITE] = "favorite";
    roles[ARCHIVE] = "archive";
    roles[PROCESSED] = "processed";
    roles[DOMAIN_URL] = "domain";
    roles[TITLE] = "title";
    roles[URL] = "url";
    roles[LEAD_IMAGE] = "lead_image";
    roles[AUTHOR] = "author";
    roles[EXCERPT] = "excerpt";
    roles[WORD_COUNT] = "word_count";
    roles[ARTICLE_ID] = "article_id";
    roles[ID] = "id";
    roles[DATE_ADDED] = "date_added";
    setRoleNames(roles);

}


void Model::setFavorite(const QModelIndex &index, const bool &value)
{
    if (index.isValid())
    {
        m_list[index.row()]->setFavorite(value);
        emit dataChanged(index, index);
    }else{
        qDebug() << "Unable set Favorite property";
    }
}



void Model::setArchive(const QModelIndex &index, const bool &value)
{
    if (index.isValid())
    {
        m_list[index.row()]->setArchive(value);
        emit dataChanged(index, index);
    }else{
        qDebug() << "Unable set Archive property";
    }
}



/* ------------------------------------------------------------------------ */


QVariant Model::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= rowCount()){
        return QVariant();
    }
    switch (role){
    case FAVORITE:
        return m_list[index.row()]->favorite();
    case ARCHIVE:
        return m_list[index.row()]->archive();
    case PROCESSED:
        return m_list[index.row()]->processed();
    case DOMAIN_URL:
        return m_list[index.row()]->domain();
    case TITLE:
        return m_list[index.row()]->title();
    case URL:
        return m_list[index.row()]->url();
    case LEAD_IMAGE:
        return m_list[index.row()]->lead_image();
    case AUTHOR:
        return m_list[index.row()]->author();
    case EXCERPT:
        return m_list[index.row()]->excerpt();
    case WORD_COUNT:
        return m_list[index.row()]->word_count();
    case ARTICLE_ID:
        return m_list[index.row()]->article_id();
    case ID:
        return m_list[index.row()]->id();
    case DATE_ADDED:
        return m_list[index.row()]->date_added();
    default:
        return QVariant();
    }

}

void Model::append(const QVariantMap &map)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_list << new ModelItem(map);
    endInsertRows();
}



void Model::appendOffline(const QVariantMap &map)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_list << new ModelItem(map, 1);
    endInsertRows();
}


void Model::insert(int index, const QVariantMap &map)
{
    beginInsertRows(QModelIndex(), 0, 0);
    m_list.insert(index, new ModelItem(map));
    endInsertRows();
}



void Model::remove(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    m_list.removeAt(index);
    endRemoveRows();
}


void Model::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount());
    m_list.clear();
    endRemoveRows();
}


QObject *Model::get(int row)
{
    if (row < 0 || row >= rowCount()) {
        return 0;
    }
    return m_list[row];
}
