#include "helper.h"
#include <QDebug>

Helper::Helper(QObject *parent) :
    QObject(parent)
{
    clipboard = QApplication::clipboard();
}



QString Helper::getText()
{
    return clipboard->text(QClipboard::Clipboard);
}


void Helper::setText(const QString &text)
{

    clipboard->setText(text);
    emit copied();
}


void Helper::sendEmail(const QString &body)
{
    QDesktopServices::openUrl(QUrl(QString("mailto:?subject=&body=%1").arg(body)));
}



void Helper::sendSms(const QString &body)
{
    QDesktopServices::openUrl(QUrl(QString("sms:?body=%1").arg(body)));
}


void Helper::openUrl(const QString &url)
{
    QUrl shareUrl("http://twitter.com/intent/tweet");
    shareUrl.addQueryItem("text", (QString("Read this - ")).toUtf8());
    shareUrl.addQueryItem("url", (QString("%1").arg(url)).toUtf8());
    shareUrl.addQueryItem("hashtags", (QString("eRBook, symbian")).toUtf8());
    QDesktopServices::openUrl(shareUrl);
}
