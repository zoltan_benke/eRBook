#include "sortmodel.h"

SortModel::SortModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
    setFilterCaseSensitivity(Qt::CaseSensitive);
    connect(this, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SIGNAL(countChanged()));
    connect(this, SIGNAL(modelReset()), this, SIGNAL(countChanged()));
}


void SortModel::setSortingBy(const QVariant &value)
{
    m_sortingBy = value.toString();
    if (m_sortingBy == "title")
        setSortRole(Model::TITLE);
    else if (m_sortingBy == "favorite")
        setSortRole(Model::FAVORITE);
    else
        setSortRole(Model::ARCHIVE);
    emit sortingByChanged();
}


void SortModel::setSortingOrder(const QVariant &value)
{
    m_sortingOrder = value.toString();
    if (m_sortingOrder == "ascending")
        QSortFilterProxyModel::sort(0, Qt::AscendingOrder);
    else
        QSortFilterProxyModel::sort(0, Qt::DescendingOrder);
    emit sortingOrderChanged();
}


bool SortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex archive = sourceModel()->index(source_row, 0, source_parent);

    if (m_filterType == "all"){
        return true;
    }
    else if (m_filterType == "archive"){
        return sourceModel()->data(archive, Model::ARCHIVE) == true;
    }
    else if (m_filterType == "non_archive"){
        return sourceModel()->data(archive, Model::ARCHIVE) == false;
    }
    return QSortFilterProxyModel::filterAcceptsRow(source_row, source_parent);

}


void SortModel::setArchive(const bool &value)
{
    QModelIndex sourceIndex = mapToSource(currentIndex());
    ((Model*)sourceModel())->setArchive(sourceIndex, value);
    emit currentItemChanged();
}


void SortModel::setFavorite(const bool &value)
{
    QModelIndex sourceIndex = mapToSource(currentIndex());
    ((Model*)sourceModel())->setFavorite(sourceIndex, value);
    emit currentItemChanged();
}



void SortModel::remove() const
{
    QModelIndex sourceIndex = mapToSource(currentIndex());
    ((Model*)sourceModel())->remove(sourceIndex.row());
}


void SortModel::setCData(const QVariant &index)
{
    if (index.toInt() >= 0){
        m_cIndex = index.toInt();
    }
    emit currentItemChanged();
}


//#include "obj/sortmodel.moc"
