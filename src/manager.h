#ifndef MANAGER_H
#define MANAGER_H

#include <QObject>
#include <QtNetwork>
#include <QtDeclarative>
#include <QVariantMap>

#include "lib/oauth.h"
#include "json/json.h"
#include "settings.h"
#include "sortmodel.h"
#include "model.h"


class Manager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool syncing READ syncing NOTIFY syncingChanged)
    Q_PROPERTY(int totalArticles READ totalArticles NOTIFY metaDataChanged)
    Q_PROPERTY(int totalPage READ totalPage NOTIFY metaDataChanged)
    Q_PROPERTY(int actualPage READ actualPage NOTIFY metaDataChanged)
public:
    explicit Manager(QObject *parent = 0);

    Settings *set;
    SortModel *sortModel;
    OAuth *auth;

    
signals:
    /* Message handler */
    void error(const QByteArray code, const QByteArray message);

    /* Token request */
    void requestTokenDone(const QUrl authUrl);
    void accessTokenDone();

    /* All request */
    void bookmarksDone();
    void readBookmarkDone(const QByteArray result);
    void addBookmarkDone(const QString result);
    void errorAddBookmark();
    void updateBookmarkDone(const QString result);
    void deleteBookmarkDone(const QString result);
    void userInfoDone(const QString result);


    /* Info banner signals */
    void banner(const QString message);


    /* RDD.ME shortener */
    void shortUrlDone(const QString url);
    void shortUrlError();
    void syncingChanged(bool syncing);
    void contentDone();
    void metaDataChanged();
    void currentSyncing(const int actualItem, const int allItem);
    
public slots:
    void requestToken();
    void accessToken(const QUrl &webUrl);

    /* All request */
    void bookmarks(const QVariant &page = "1", const QVariant &per_page = "50");
    void offlineBookmarks();
    void readBookmark(const QVariant &id = "");
    void addBookmark(const QString &urlString, const int &favorite, const int &archive);
    void updateBookmark(const int &favorite, const int &archive);
    void deleteBookmark();
    void userInfo();


    /* RDD.ME shortener */
    void shortUrl(const QString &sourceUrl);






private slots:
    void handleErrors(const int &errorCode);
    void requestTokenFinished();
    void accessTokenFinished();

    /* All request */
    void bookmarksFinished();
    void readBookmarkFinished();
    void addBookmarkFinished();
    void updateBookmarkFinished();
    void deleteBookmarkFinished();
    void userInfoFinished();


    /* RDD.ME shortener */
    void shortUrlFinished();

    void syncingStart();
    void bookmarkContentDone(const QByteArray result);


    inline bool syncing() const { return _syncing; }
    inline void setSyncing(const bool &value){
        if (_syncing != value){
            _syncing = value;
            emit syncingChanged(_syncing);
        }
    }
    inline int totalArticles() const { return _totalArticles; }
    inline int totalPage() const { return _totalPage; }
    inline int actualPage() const { return _actualPage; }



private:
    QNetworkAccessManager *manager;
    QString m_tokenTemp,
    m_secretTemp;
    Model *model;
    JsonReader *reader;
    bool _syncing;
    int _totalArticles,
    _totalPage,
    _actualPage;
    
};

QML_DECLARE_TYPE(Manager)

#endif // MANAGER_H
