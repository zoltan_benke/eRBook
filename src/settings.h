#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QtSql>
#include <QVariantMap>

#ifdef QT_DEBUG
#include <QDebug>
#endif

class Settings : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool authorized READ getAuthorized NOTIFY authorizedChanged)
    Q_PROPERTY(bool theme READ theme WRITE setTheme NOTIFY themeChanged)
    Q_PROPERTY(int font READ font WRITE setFont NOTIFY fontChanged)
    Q_PROPERTY(QString sortBy READ sortBy WRITE setSortBy NOTIFY sortByChanged)
    Q_PROPERTY(QString sortOrder READ sortOrder WRITE setSortOrder NOTIFY sortOrderChanged)
    Q_PROPERTY(QString viewMode READ viewMode WRITE setViewMode NOTIFY viewModeChanged)
    Q_PROPERTY(bool debug READ debug WRITE setDebug NOTIFY debugChanged)
    Q_PROPERTY(bool sync READ sync WRITE setSync NOTIFY syncChanged)
    Q_PROPERTY(bool dbEmpty READ dbEmpty NOTIFY dbEmptyChanged)
public:
    explicit Settings(QObject *parent = 0);
    virtual ~Settings();


signals:
    void authorizedChanged();
    void dbRestored();
    void authRestored();
    void themeChanged(const bool theme);
    void fontChanged();
    void sortByChanged(const QString sortBy);
    void sortOrderChanged(const QString sortOrder);
    void viewModeChanged(const QString viewMode);
    void debugChanged();
    void syncChanged();
    void dbEmptyChanged();


public slots:
    bool setAuth(const QVariant &token, const QVariant &secret);

    /* QML properties */
    QVariant getToken();
    QVariant getSecret();

    /* Bookmark cacher data */
    bool setBookmark(const QVariant &id, const QVariant &content);
    bool setBookmarkCache(const QVariantMap &map);
    bool setArchiveCache(const QVariant &id, const QVariant &archive);
    bool setFavoriteCache(const QVariant &id, const QVariant &favorite);
    bool setShortUrl(const QVariant &id, const QVariant &url);
    bool shortUrlExists(const QVariant &id);
    QString shortUrl(const QVariant &id);
    bool bookmarkExists(const QVariant &id);
    bool contentExists(const QVariant &id);
    QList<QVariantMap> getBookmarks();

    int position(const QVariant &id);
    QVariant content(const QVariant &id);
    void setPosition(const QVariant &id, const QVariant &position);
    bool deleteFromCacher(const QVariant &id);


    bool sync();


    bool restoreAuth();
    bool restoreDB();
    bool dbEmpty();





protected:
    void initialize();
    bool getAuthorized();

    bool theme();
    void setTheme(const QVariant &value);
    int font();
    void setFont(const int &value);
    QString sortBy();
    void setSortBy(const QVariant &value);
    QString sortOrder();
    void setSortOrder(const QVariant &value);
    QString viewMode();
    void setViewMode(const QVariant &value);
    bool debug();
    void setDebug(const QVariant &value);
    void setSync(const QVariant &value);



private:
    QSqlDatabase m_database;

};

#endif // SETTINGS_H
