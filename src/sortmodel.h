#ifndef SORTMODEL_H
#define SORTMODEL_H

#include <QSortFilterProxyModel>
#include <QObject>
#include "model.h"

class SortModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(int cIndex READ cIndex NOTIFY currentItemChanged)
    Q_PROPERTY(bool cFavorite READ cFavorite WRITE setFavorite NOTIFY currentItemChanged)
    Q_PROPERTY(bool cArchive READ cArchive WRITE setArchive NOTIFY currentItemChanged)
    Q_PROPERTY(bool cProcessed READ cProcessed NOTIFY currentItemChanged)
    Q_PROPERTY(QString cDomain READ cDomain NOTIFY currentItemChanged)

    Q_PROPERTY(QString cTitle READ cTitle NOTIFY currentItemChanged)
    Q_PROPERTY(QString cUrl READ cUrl NOTIFY currentItemChanged)
    Q_PROPERTY(QString cLeadImage READ cLeadImage NOTIFY currentItemChanged)
    Q_PROPERTY(QString cAuthor READ cAuthor NOTIFY currentItemChanged)
    Q_PROPERTY(QString cExcerpt READ cExcerpt NOTIFY currentItemChanged)
    Q_PROPERTY(QString cWordCount READ cWordCount NOTIFY currentItemChanged)
    Q_PROPERTY(QString cArticleId READ cArticleId NOTIFY currentItemChanged)
    Q_PROPERTY(QString cId READ cId NOTIFY currentItemChanged)
    Q_PROPERTY(QString cDateAdded READ cDateAdded NOTIFY currentItemChanged)

    Q_PROPERTY(QString filterType READ filterType WRITE setFilterType NOTIFY filterTypeChanged)
    Q_PROPERTY(QString sortingBy READ sortingBy WRITE setSortingBy NOTIFY sortingByChanged)
    Q_PROPERTY(QString sortingOrder READ sortingOrder WRITE setSortingOrder NOTIFY sortingOrderChanged)
public:
    explicit SortModel(QObject *parent = 0);

    int count() const { return QSortFilterProxyModel::rowCount(); }


signals:
    void currentItemChanged();
    void countChanged();
    void filterTypeChanged();
    void sortingByChanged();
    void sortingOrderChanged();


public slots:
    void setCData(const QVariant &index);
    inline void reSort() { sort(0, sortOrder()); invalidateFilter(); }

    inline int cIndex() const { return m_cIndex; }
    inline bool cFavorite() { return data(currentIndex(), Model::FAVORITE).toBool(); }
    inline bool cArchive() { return data(currentIndex(), Model::ARCHIVE).toBool(); }
    inline bool cProcessed() { return data(currentIndex(), Model::PROCESSED).toBool(); }
    inline QString cDomain() { return data(currentIndex(), Model::DOMAIN_URL).toString(); }

    inline QString cTitle() { return data(currentIndex(), Model::TITLE).toString(); }
    inline QString cUrl() { return data(currentIndex(), Model::URL).toString(); }
    inline QString cLeadImage() { return data(currentIndex(), Model::LEAD_IMAGE).toString(); }
    inline QString cAuthor() { return data(currentIndex(), Model::AUTHOR).toString(); }
    inline QString cExcerpt() { return data(currentIndex(), Model::EXCERPT).toString(); }
    inline QString cWordCount() { return data(currentIndex(), Model::WORD_COUNT).toString(); }
    inline QString cArticleId() { return data(currentIndex(), Model::ARTICLE_ID).toString(); }
    inline QString cId() { return data(currentIndex(), Model::ID).toString(); }
    inline QString cDateAdded() { return data(currentIndex(), Model::DATE_ADDED).toString(); }


    inline void clear() const { ((Model*)sourceModel())->clear(); }
    inline void append(const QVariantMap &map) const {
        ((Model*)sourceModel())->append(map);
    }
    inline void appendOffline(const QVariantMap &map) const {
        ((Model*)sourceModel())->appendOffline(map);
    }
    void remove() const;



private slots:
    inline QString filterType() const { return m_filterType; }
    inline QString sortingBy() const { return m_sortingBy; }
    inline QString sortingOrder() const { return m_sortingOrder; }
    void setSortingBy(const QVariant &value);
    void setSortingOrder(const QVariant &value);
    inline void setFilterType(const QString type) {
        beginResetModel();
        if (m_filterType != type){
            m_filterType = type;
            invalidateFilter();
        }
        endResetModel();
    }
    inline QModelIndex currentIndex() const { return this->index(cIndex(), 0); }
    void setArchive(const bool &value);
    void setFavorite(const bool &value);



protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;


private:
    QString m_filterType,
    m_sortingBy,
    m_sortingOrder;
    int m_cIndex;

};

#endif // SORTMODEL_H
