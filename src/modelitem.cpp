#include "modelitem.h"


ModelItem::ModelItem(const QVariantMap &map)
{
    m_favorite = map["favorite"].toBool();
    m_archive = map["archive"].toBool();
    m_processed = map["article"].toMap()["processed"].toBool();
    m_domain = map["article"].toMap()["domain"].toString();
    m_title = map["article"].toMap()["title"].toString();
    m_url = map["article"].toMap()["url"].toString();
    m_lead_image_url = map["article"].toMap()["lead_image_url"].toString();
    m_author = map["article"].toMap()["author"].toString();

    m_excerpt = map["article"].toMap()["excerpt"].toString();
    m_word_count = map["article"].toMap()["word_count"].toString();
    m_article_id = map["article"].toMap()["id"].toString();
    m_id = map["id"].toString();
    m_date_added = map["date_added"].toString();
}


ModelItem::ModelItem(const QVariantMap &map, const int offline)
{
    Q_UNUSED(offline)
    m_favorite = map["favorite"].toBool();
    m_archive = map["archive"].toBool();
    m_processed = map["processed"].toBool();

    m_domain = map["domain"].toString();
    m_title = map["title"].toString();
    m_url = map["url"].toString();
    m_lead_image_url = map["lead_image"].toString();
    m_author = map["author"].toString();
    m_excerpt = map["excerpt"].toString();
    m_word_count = map["word_count"].toString();
    m_article_id = map["id"].toString();
    m_id = map["orig_id"].toString();
    m_date_added = map["date"].toString();
}
