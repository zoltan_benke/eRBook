#include "settings.h"
#include <QApplication>

Settings::Settings(QObject *parent) :
    QObject(parent)
{
    QString path;
    m_database = QSqlDatabase::addDatabase("QSQLITE");
#ifdef Q_OS_HARMATTAN
    QDir dir;
    dir.mkpath("/home/user/eRBook/");
    path.append("/home/user/eRBook/erbook.sqlite");
    m_database.setDatabaseName(path);
#ifdef QT_DEBUG
    qDebug() << "DATABASE PATH:" << path;
#endif
#elif defined(Q_OS_SYMBIAN)
    path.append(qApp->applicationDirPath());
    path.append(QDir::separator()).append("erbook.sqlite");
    path = QDir::toNativeSeparators(path);
    m_database.setDatabaseName(path);
#ifdef QT_DEBUG
    qDebug() << "DATABASE PATH:" << path;
#endif
#else
    path.append("../eRBook/database/erbook.sqlite");
    m_database.setDatabaseName(path);
    qDebug() << "DATABASE PATH:" << path;
#endif
    initialize();
}


Settings::~Settings()
{
    if (m_database.isOpen()){
        m_database.close();
    }
}

/* QML property
/
/
/
*/
QVariant Settings::getToken()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("SELECT token FROM auth");

    while (query.next()){
        value = query.value(0);
    }

    return value;
}


QVariant Settings::getSecret()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("SELECT secret FROM auth");

    while (query.next()){
        value = query.value(0);
    }

    return value;
}


bool Settings::getAuthorized()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("SELECT secret FROM auth");
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return false;
    }
    return true;
}


void Settings::initialize()
{
    if (m_database.open()){
        m_database.exec("CREATE TABLE IF NOT EXISTS auth (token TEXT UNIQUE, secret TEXT UNIQUE)");
        m_database.exec("CREATE TABLE IF NOT EXISTS cacher (id TEXT UNIQUE, favorite TEXT, domain TEXT, title TEXT, date TEXT, archive TEXT, url TEXT, processed TEXT, lead_image TEXT, author TEXT, excerpt TEXT, word_count TEXT, orig_id TEXT, content TEXT, position TEXT, shortUrl TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS theme (value TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS font (value INTEGER)");
        m_database.exec("CREATE TABLE IF NOT EXISTS sortBy (value TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS sortOrder (value TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS viewMode (value TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS debug (value TEXT)");
        m_database.exec("CREATE TABLE IF NOT EXISTS sync (value TEXT)");

    }else{
        qDebug() << m_database.lastError();
    }
}



bool Settings::restoreDB()
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("DELETE FROM cacher");

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO RESTORE cacher";
    }

    if (success){
        emit dbRestored();
        emit dbEmptyChanged();
    }
    return success;
}


bool Settings::restoreAuth()
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM cacher");
    query.prepare("DELETE FROM auth");

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO RESTORE auth";
    }

    if (success){
        emit authRestored();
    }
    return success;
}



bool Settings::dbEmpty()
{
    QVariant isEmpty;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM cacher");

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO GET COUNT from cacher";
    }

    while (query.next()){
         isEmpty = query.value(0);
    }
    if (isEmpty.toInt() == 0){
        return true;
    }else{
        return false;
    }
}



bool Settings::setAuth(const QVariant &token, const QVariant &secret)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM auth");
    query.prepare("INSERT OR REPLACE INTO auth VALUES(?, ?)");
    query.addBindValue(token.toString());
    query.addBindValue(secret.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO INSERT VALUES";
    }

    emit authorizedChanged();
    return success;
}


bool Settings::setBookmarkCache(const QVariantMap &map)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("INSERT OR REPLACE INTO cacher (id, favorite, domain, title, date, archive, url, processed, lead_image, author, excerpt, word_count, orig_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)");
    query.addBindValue(map["article"].toMap()["id"].toString());
    query.addBindValue(map["favorite"].toString());
    query.addBindValue(map["article"].toMap()["domain"].toString());
    query.addBindValue(map["article"].toMap()["title"].toString());
    query.addBindValue(map["date_added"].toString());
    query.addBindValue(map["archive"].toString());
    query.addBindValue(map["article"].toMap()["url"].toString());
    query.addBindValue(map["article"].toMap()["processed"].toString());
    query.addBindValue(map["article"].toMap()["lead_image_url"].toString());
    query.addBindValue(map["article"].toMap()["author"].toString());
    query.addBindValue(map["article"].toMap()["excerpt"].toString());
    query.addBindValue(map["article"].toMap()["word_count"].toString());
    query.addBindValue(map["id"].toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO INSERT CACHE BOOKMARK to CACHER table";
    }else{
        emit dbEmptyChanged();
    }

    return success;
}



bool Settings::setArchiveCache(const QVariant &id, const QVariant &archive)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE cacher SET archive = ? WHERE id = ?");
    query.addBindValue(archive.toString());
    query.addBindValue(id.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO UPDATE archive value to CACHER table";
    }

    return success;
}



bool Settings::setFavoriteCache(const QVariant &id, const QVariant &favorite)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE cacher SET favorite = ? WHERE id = ?");
    query.addBindValue(favorite.toString());
    query.addBindValue(id.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO UPDATE favorite value to CACHER table";
    }

    return success;
}




bool Settings::setBookmark(const QVariant &id, const QVariant &content)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE cacher SET content = ? WHERE id = ?");
    query.addBindValue(content.toString());
    query.addBindValue(id.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO INSERT VALUES id and bookmark to CACHER table";
    }

    return success;
}



QList<QVariantMap> Settings::getBookmarks()
{
    //id, favorite, domain, title, date, archive
    QVariantMap result;
    QList<QVariantMap> list;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    //query.prepare("SELECT id, favorite, domain, title, date, archive, url, processed, lead_image, author, excerpt, word_count, orig_id FROM cacher");
    query.prepare("SELECT * FROM cacher");

    bool success = query.exec();

    while (query.next()){
        result["id"] = query.value(0).toString();
        result["favorite"] = query.value(1);
        result["domain"] = query.value(2).toString();
        result["title"] = query.value(3).toString();
        result["date"] = query.value(4).toString();
        result["archive"] = query.value(5);
        result["url"] = query.value(6).toString();
        result["processed"] = query.value(7);
        result["lead_image"] = query.value(8).toString();
        result["author"] = query.value(9).toString();
        result["excerpt"] = query.value(10).toString();
        result["word_count"] = query.value(11).toString();
        result["orig_id"] = query.value(12).toString();
        list.append(result);
    }
    if (!success){
        qWarning() << "UNABLE GET BOOKMARKS from CACHER table";
    }

    return list;
}



QString Settings::shortUrl(const QVariant &id)
{
    QString value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT shortUrl FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());
    query.exec();
    while (query.next()){
        value = query.value(0).toString();
    }

    return value;
}



bool Settings::setShortUrl(const QVariant &id, const QVariant &url)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE cacher SET shortUrl = ? WHERE id = ?");
    query.addBindValue(url.toString());
    query.addBindValue(id.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO INSERT SHORTURL to CACHER table";
    }

    return success;
}



bool Settings::shortUrlExists(const QVariant &id)
{
    QString value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT shortUrl FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO GET shortUrl from CACHER table";
    }

    while (query.next()){
        value = query.value(0).toString();
    }
    if (value.isEmpty()){
        return false;
    }
    return true;
}


bool Settings::bookmarkExists(const QVariant &id)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT id FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO GET id from CACHER table";
    }

    if (success && query.next()){
        return true;
    }
    return false;
}



bool Settings::contentExists(const QVariant &id)
{
    QString value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT content FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO GET id from CACHER table";
    }

    while (query.next()){
        value = query.value(0).toString();
    }
    if (value.isEmpty()){
        return false;
    }
    return true;
}



int Settings::position(const QVariant &id)
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT position FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    return value.toInt();
}



void Settings::setPosition(const QVariant &id, const QVariant &position)
{
    bool success;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("UPDATE cacher SET position = ? WHERE id = ?");
    query.addBindValue(position.toString());
    query.addBindValue(id.toString());

    success = query.exec();

    if (!success){
        qWarning() << "UNABLE UPDATE position INTO CACHER table";
    }

}


QVariant Settings::content(const QVariant &id)
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT content FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    return value;
}



bool Settings::deleteFromCacher(const QVariant &id)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("DELETE FROM cacher WHERE id = ?");
    query.addBindValue(id.toString());

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE TO DELETE from CACHER table";
    }

    return success;
}



bool Settings::theme()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM theme");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
#ifdef Q_OS_HARMATTAN
        return QVariant("false").toBool();
#else
        return QVariant("true").toBool(); // zmenit na true
#endif
    }
    return value.toBool();
}



void Settings::setTheme(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM theme");
    query.prepare("INSERT OR REPLACE INTO theme VALUES (?)");
    query.addBindValue(value.toString());

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE set value to THEME table";
    }

    if (success){
        emit themeChanged(value.toBool());
    }
}



int Settings::font()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM font");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
#ifdef Q_OS_HARMATTAN
        return QVariant("22").toInt();
#else
        return QVariant("18").toInt(); // Zmenit na 18 pri symbiane
#endif
    }
    return value.toInt();
}



void Settings::setFont(const int &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM font");
    query.prepare("INSERT OR REPLACE INTO font VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE set value to FONT table";
    }

    if (success){
        emit fontChanged();
    }
}



QString Settings::sortBy()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM sortBy");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return QVariant("title").toString();
    }
    return value.toString();
}




void Settings::setSortBy(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM sortBy");
    query.prepare("INSERT OR REPLACE INTO sortBy VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE set value to SORTBY table";
    }

    if (success){
        emit sortByChanged(value.toString());
    }
}




QString Settings::sortOrder()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM sortOrder");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return QVariant("ascending").toString();
    }
    return value.toString();
}



void Settings::setSortOrder(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM sortOrder");
    query.prepare("INSERT OR REPLACE INTO sortOrder VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE set value to sortOrder table";
    }

    if (success){
        emit sortOrderChanged(value.toString());
    }
}



QString Settings::viewMode()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM viewMode");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return QVariant("all").toString();
    }
    return value.toString();
}


void Settings::setViewMode(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM viewMode");
    query.prepare("INSERT OR REPLACE INTO viewMode VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE UPDATE VIEWMODE table";
    }

    if (success){
        emit viewModeChanged(value.toString());
    }
}



bool Settings::debug()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM debug");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return QVariant("false").toBool();
    }
    return value.toBool();
}


void Settings::setDebug(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM debug");
    query.prepare("INSERT OR REPLACE INTO debug VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE UPDATE DEBUG table";
    }

    if (success){
        emit debugChanged();
    }
}



bool Settings::sync()
{
    QVariant value;
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.prepare("SELECT value FROM sync");
    query.exec();
    while (query.next()){
        value = query.value(0);
    }

    if (value.isNull()){
        return QVariant("true").toBool();
    }
    return value.toBool();
}


void Settings::setSync(const QVariant &value)
{
    if (!m_database.isOpen()){
        m_database.open();
    }
    QSqlQuery query;
    query.exec("DELETE FROM sync");
    query.prepare("INSERT OR REPLACE INTO sync VALUES (?)");
    query.addBindValue(value);

    bool success = query.exec();

    if (!success){
        qWarning() << "UNABLE UPDATE SYNC table";
    }

    if (success){
        emit syncChanged();
    }
}
