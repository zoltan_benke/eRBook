#ifndef MODEL_H
#define MODEL_H

#include <QAbstractListModel>
#include <QString>
#include "modelitem.h"
#include <QDebug>

class Model : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit Model(QObject *parent = 0);

    enum MY_ROLES{
        FAVORITE = Qt::UserRole + 1,
        ARCHIVE,
        PROCESSED,
        DOMAIN_URL,
        TITLE,
        URL,
        LEAD_IMAGE,
        AUTHOR,
        EXCERPT,
        WORD_COUNT,
        ARTICLE_ID,
        ID,
        DATE_ADDED
    };


    inline int rowCount(const QModelIndex &parent = QModelIndex()) const { Q_UNUSED(parent) return m_list.size(); }
    QVariant data(const QModelIndex &index, int role) const;


public slots:
    /* For sections scroller */
    QObject* get(int row);
    void append(const QVariantMap &map);
    void appendOffline(const QVariantMap &map);
    void insert(int index, const QVariantMap &map);
    void remove(int index);
    void clear();


    void setFavorite(const QModelIndex &index, const bool &value);
    void setArchive(const QModelIndex &index, const bool &value);


private:
    QList <ModelItem*> m_list;
    int m_cIndex;

};

#endif // MODEL_H
